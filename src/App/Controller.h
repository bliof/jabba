/*
 * Controller.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Controller_h__
#define __Controller_h__

#include "Jabba/CsvParser.h"
#include "Jabba/ExprParser.h"
#include "Jabba/Table.h"

#include "Menu.h"

namespace App {
    class Controller {
        public:

        static Controller* get_instance();

        virtual ~Controller();

        void run();

        void load_csv_file(const std::string& filename);

        static bool menu_show_table();
        static bool menu_evaluate_table();
        static bool menu_change_cell();
        static bool menu_save_to_file();
        static bool menu_exit();

        private:

        Jabba::Table* table;
        Jabba::CsvParser csv_parser;
        Jabba::ExprParser expr_parser;
        std::string data_file;
        Menu menu;

        void dump_result_vector(
            const std::vector< std::vector< std::string > > &result,
            const std::vector< size_t > &columns_sizes
        );

        static Controller* instance;

        Controller();

        Controller(const Controller &other) = delete;

        Controller& operator=(const Controller &other) = delete;
    };
}

#endif /* !__Controller_h__ */

