/*
 * Base.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __App_Exception_Base_h__
#define __App_Exception_Base_h__

#include <exception>

namespace App {
    namespace Exception {
        class Base : public std::runtime_error {
            public:

            Base(std::string const& msg)
             : std::runtime_error(msg)
            {}
        };
    }
}

#endif /* !__App_Exception_Base_h__ */

