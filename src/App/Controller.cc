/*
 * Controller.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Controller.h"

#include <fstream>
#include <iostream>
#include <iomanip>

#include "Jabba/Table.h"
#include "Jabba/VectorTable.h"
#include "Jabba/Expr.h"
#include "Jabba/Exception/Base.h"

#include "Utils.h"

namespace App {
    Controller* Controller::instance = 0;

    Controller::Controller()
        : table(new Jabba::VectorTable)
    {
        {
            Menu::Option option;
            option.label = "Show table";
            option.key = '0';
            option.handler = menu_show_table;
            menu.add_option(option);
        }

        {
            Menu::Option option;
            option.label = "Evaluate table";
            option.key = '1';
            option.handler = menu_evaluate_table;
            menu.add_option(option);
        }

        {
            Menu::Option option;
            option.label = "Change cell";
            option.key = '2';
            option.handler = menu_change_cell;
            menu.add_option(option);
        }

        {
            Menu::Option option;
            option.label = "Save to file";
            option.key = '3';
            option.handler = menu_save_to_file;
            menu.add_option(option);
        }

        {
            Menu::Option option;
            option.label = "Exit";
            option.key = 'q';
            option.handler = menu_exit;
            menu.add_option(option);
        }
    }

    Controller::~Controller() {
        delete table;
    }

    Controller* Controller::get_instance() {
        if (!instance) {
            instance = new Controller;
        }
        return instance;
    }

    void Controller::run() {
        menu.show();
    }

    void Controller::load_csv_file(const std::string& filename) {
        std::ifstream file(filename.c_str());

        if (file) {
            try {
                csv_parser.parse(file, *table);
            } catch(...) {
                file.close();
                throw;
            }
        }

        file.close();
    }

    void Controller::dump_result_vector(
        const std::vector< std::vector< std::string > > &result,
        const std::vector< size_t > &columns_sizes
    ) {
        for (size_t row = 0; row < result.size(); row++) {
            size_t column;
            for (column = 0; column < result[row].size(); column++) {
                std::cout << std::setw(columns_sizes[column] + 3) << result[row][column] << "|";
                std::cout.flush();
            }

            for (; column < columns_sizes.size(); column++) {
                std::cout << std::setw(columns_sizes[column] + 4) << "|";
            }

            std::cout << std::endl;
        }
    }

    bool Controller::menu_show_table() {
        Controller* controller = Controller::get_instance();

        Jabba::Table* table = controller->table;

        std::vector< std::vector< std::string > > rows;

        std::vector<size_t> columns_sizes;

        for (size_t row = 0; row < table->rows_count(); row++) {
            std::vector< std::string > columns;

            for (size_t column = 0; column < table->columns_count(row); column++) {
                Jabba::Expr* expr = table->get_cell(row, column);

                std::string data = "";
                if (expr) {
                    data = expr->to_string();
                }

                if (column >= columns_sizes.size()) {
                    columns_sizes.resize(column + 1, 1);
                }

                if (data.length() > columns_sizes[column]) {
                    columns_sizes[column] = data.length();
                }

                columns.push_back(data);
            }

            rows.push_back(columns);
        }

        controller->dump_result_vector(rows, columns_sizes);

        return true;
    }

    bool Controller::menu_evaluate_table() {
        Controller* controller = Controller::get_instance();

        Jabba::Table* table = controller->table;

        std::vector< std::vector< std::string > > rows;

        std::vector<size_t> columns_sizes;

        for (size_t row = 0; row < table->rows_count(); row++) {
            std::vector< std::string > columns;

            for (size_t column = 0; column < table->columns_count(row); column++) {
                Jabba::Expr* expr = table->get_cell(row, column);

                std::string data = "";
                if (expr) {
                    data = expr->evaluate_to_string(table);
                }

                if (column >= columns_sizes.size()) {
                    columns_sizes.resize(column + 1, 1);
                }

                if (data.length() > columns_sizes[column]) {
                    columns_sizes[column] = data.length();
                }

                columns.push_back(data);
            }

            rows.push_back(columns);
        }

        controller->dump_result_vector(rows, columns_sizes);

        return true;
    }

    bool Controller::menu_change_cell() {
        Controller* controller = Controller::get_instance();

        Jabba::Table* table = controller->table;

        int row;
        utl::read_user_input<int>("row: ", row);

        int column;
        utl::read_user_input<int>("column: ", column);

        std::string expr_str;

        if (row < table->rows_count() && column < table->columns_count(row)) {
            Jabba::Expr* current = table->get_cell(row, column);

            if (current) {
                std::cout << "current: " << current->to_string() << std::endl;
            }
        }

        Jabba::Expr* expr = NULL;
        while (true) {
            utl::read_user_input("expression: ", expr_str);

            try {
                expr = controller->expr_parser.parse(expr_str);
                break;
            } catch(Jabba::Exception::Base& e) {
                std::cout << e.what() << std::endl;
            } catch(...) {
                throw;
            }
        }

        table->set_cell(row, column, expr);

        return true;
    }

    bool Controller::menu_save_to_file() {
        Controller* controller = Controller::get_instance();
        Jabba::Table* table = controller->table;

        std::string filename;
        utl::read_user_input("file: ", filename);

        ofstream file(filename.c_str());

        if (file) {
            for (size_t row = 0; row < table->rows_count(); row++) {
                for (size_t column = 0; column < table->columns_count(row); column++) {
                    Jabba::Expr* expr = table->get_cell(row, column);

                    if (expr) {
                        if (expr->is_formula()) {
                            file << "=";
                        }

                        file << expr->to_string() << ",";
                    } else {
                        file << ",";
                    }
                }

                file << std::endl;
            }

            file.close();
        } else {
            std::cout << "could not open " << filename << std::endl;
        }

        return true;
    }

    bool Controller::menu_exit() {
        return false;
    }
}

