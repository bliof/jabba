/*
 * Utils.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __App_Utils_h__
#define __App_Utils_h__

#include <string>
#include <iostream>
#include <limits>
#include <ios>

#include "Exception/Base.h"

namespace App {
    namespace utl {
        inline void clear_cin() {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

        inline void read_user_input(const std::string& label, std::string& into) {
            while (true) {
                //clear_cin();
                std::cout << label;

                std::getline(std::cin, into);
                if (std::cin) {
                    break;
                } else if(std::cin.eof()) {
                    throw Exception::Base(std::string("Exiting because of eof"));
                } else {
                    std::cout << "Bad input!" << std::endl;
                    clear_cin();
                }
            }
        }

        template <typename T>
        void read_user_input(const std::string& label, T& into) {
            while (true) {
                //clear_cin();
                std::cout << label;

                if (std::cin >> into) {
                    clear_cin();
                    break;
                } else if(std::cin.eof()) {
                    throw Exception::Base(std::string("Exiting because of eof"));
                } else {
                    std::cout << "Bad input!" << std::endl;
                    clear_cin();
                }
            }
        }
    }
}

#endif /* !__Utils_h__ */

