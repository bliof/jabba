/*
 * Menu.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Menu_h__
#define __Menu_h__

#include <iostream>
#include <vector>

using namespace std;

namespace App {
    class Menu {
        public:

        Menu () {};
        virtual ~Menu () {};

        typedef bool (*pHandler)();
        struct Option {
            string label;
            char key;
            pHandler handler;
        };

        vector<Option> options;

        Menu& add_option(Option opt);

        void show();
    };
}

#endif /* !__Menu_h__ */

