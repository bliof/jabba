/*
 * Menu.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Menu.h"

#include <vector>
#include <iostream>
#include <string>

#include "Utils.h"

using namespace std;

namespace App {
    void Menu::show() {
        bool loop = true;

        while(loop) {
            cout << endl;
            for (vector<Option>::iterator current = options.begin(); current != options.end(); ++current) {
                cout << "[" << current->key << "] " << current->label << endl;
            }
            cout << endl;

            char input;

            utl::read_user_input("Choose option: ", input);
            cout << endl;

            if (!(isalnum(input))) { continue; }

            vector<Option>::iterator choice;
            for (choice = this->options.begin(); choice != this->options.end(); ++choice) {
                if (choice->key == input) {
                    loop = choice->handler();
                    break;
                }
            }
        }
    }

    Menu& Menu::add_option(Menu::Option opt) {
        options.push_back(opt);
        return *this;
    }
}

