/*
 * main.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <string>
#include <iostream>
#include <fstream>

#include "Controller.h"
#include "Exception/Base.h"
#include "Jabba/Exception/Base.h"

int main(int argc, const char *argv[]) {
    std::string filename = "";

    if (argc > 1) {
        filename = argv[1];
    } else {
        std::cout << "Enter data file:";
        std::cin >> filename;
    }

    std::ifstream f(filename.c_str());
    if (f) {
        f.close();
    } else {
        std::cout << "Cannot find file: " << filename << std::endl;
        return 1;
    }

    App::Controller *app = NULL;
    try {
        app = App::Controller::get_instance();
        app->load_csv_file(filename);
        app->run();
    } catch(Jabba::Exception::Base& e) {
        std::cerr << e.what() << std::endl;
    } catch(App::Exception::Base& e) {
        std::cout << std::endl << e.what() << std::endl;
    } catch(...) {
        std::cout << std::endl << "Internal error! Please contact administrator!" << std::endl;
        std::cerr << "Unknown error occurred" << std::endl;
    }
    if (app) {
        delete app;
    }

    return 0;
}

