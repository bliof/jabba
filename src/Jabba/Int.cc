/*
 * Int.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Int.h"

#include <string>

#include "Expr.h"
#include "Utils.h"
#include "Table.h"

namespace Jabba {
    Int::Int()
        : value_(0)
    {}

    Int::Int(int value)
        : value_(value)
    {}

    Int::Int(double value)
        : value_(value + 0.5)
    {}

    Int::Int(const Expr &expr) {
        value_ = expr.evaluate(NULL) + 0.5;
    }

    double Int::evaluate(const Table* table) const {
        return value_;
    }

    std::string Int::to_string() const {
        return utl::to_string(value_);
    }

    Expr* Int::clone() const {
        return new Int(value_);
    }
}

