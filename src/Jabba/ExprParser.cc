/*
 * ExprParser.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "ExprParser.h"

#include <string>
#include <sstream>
#include <iostream>

#include "Utils.h"

#include "Exception/ParseError.h"

#include "Expr.h"

#include "Int.h"
#include "Double.h"
#include "String.h"
#include "Negation.h"

#include "Variable.h"
#include "Multiplication.h"
#include "Division.h"
#include "Addition.h"
#include "Power.h"

namespace Jabba {
    Expr* ExprParser::parse(std::string raw_expr) {
        utl::trim(raw_expr);

        if (raw_expr.empty()) {
            return NULL;
        }

        if (utl::is_number(raw_expr)) {
            return parse_number(raw_expr);
        } else if(raw_expr[0] == '=') {
            return parse_formula(raw_expr.substr(1));
        } else {
            std::string string;
            if (raw_expr[0] == '"') {
                string = raw_expr.substr(1, raw_expr.length() - 2);
            } else {
                string = raw_expr;
            }
            return new Jabba::String(string);
        }

        return NULL;
    }

    Expr* ExprParser::extract_number(std::string formula, int &i) {
        while (formula[i] == ' ') {
            i++;
        }

        int end_of_number = formula.find_first_not_of("0123456789.", i);

        std::string number_to_parse;

        if (end_of_number > 0) {
            number_to_parse = formula.substr(i, end_of_number - i);
            i = end_of_number;
        } else {
            number_to_parse = formula.substr(i);
            i = formula.length();
        }

        return parse_number(number_to_parse);
    }

    Expr* ExprParser::parse_number(std::string number) {
        if (number.find(".") == std::string::npos) {
            return new Jabba::Int(utl::to_number<int>(number));
        } else {
            return new Jabba::Double(utl::to_number<double>(number));
        }
    }

    Expr* ExprParser::parse_formula(std::string formula) {
        utl::trim(formula);

        Expr* left = NULL;
        Expr* right = NULL;
        Function function = UNDEF;

        int i = 0;

        try {
            while (i < formula.length()) {
                if (formula[i] == '+') {

                    if (!left || right) {
                        throw Exception::ParseError(formula, i, "bad addition");
                    }

                    function = ADDITION;

                } else if (formula[i] == '-') {

                    if (!left || right) {
                        throw Exception::ParseError(formula, i, "bad subtraction");
                    }

                    function = SUBTRACTION;

                } else if (formula[i] == '*') {

                    if (!left || right) {
                        throw Exception::ParseError(formula, i, "bad multiplication");
                    }

                    function = MULTIPLICATION;

                } else if (formula[i] == '/') {

                    if (!left || right) {
                        throw Exception::ParseError(formula, i, "bad division");
                    }

                    function = DIVISION;

                } else if (formula[i] == '^') {

                    if (!left || right) {
                        throw Exception::ParseError(formula, i, "bad power");
                    }

                    function = POWER;

                } else if (formula[i] == ' ') {
                } else {
                    Expr* current = parse_formula_argument(formula, i);

                    if (!left) {
                        left = current;
                    } else {
                        right = current;

                        Expr* expr = build_expr(function, left, right);

                        delete left; left = NULL;
                        delete right; right = NULL;
                        function = UNDEF;

                        if (expr) {
                            left = expr;
                        } else {
                            throw Exception::ParseError(formula, i, "bad expression");
                        }
                    }

                    continue;
                }

                i++;
            }

            if (function != UNDEF) {
                throw Exception::ParseError(formula, i, "not enough arguments for expression");
            }
        } catch(Exception::ParseError e) {
            delete left;
            delete right;

            throw;
        }

        return left;
    }

    Expr* ExprParser::parse_formula_argument(std::string formula, int &i) {
        Expr* result;

        if (formula[i] == 'R') {
            result = parse_variable(formula, i);
        } else if (formula[i] == '(') {
            result = parse_formula_in_brackets(formula, i);
        } else {
            result = extract_number(formula, i);
        }

        return result;
    }

    Expr* ExprParser::parse_formula_in_brackets(std::string formula, int &i) {
        Expr* result;

        int other_bracket = utl::find_matching_closing_bracket(formula, i);

        if (other_bracket <= 0) {
            throw Exception::ParseError(formula, i, "unmatched bracket");
        }

        try {
            result = parse_formula(formula.substr(i + 1, other_bracket - i - 1));
        } catch(Exception::ParseError e) {
            throw Exception::ParseError(formula, i + e.position, e.cause);
        }

        if (!result) {
            throw Exception::ParseError(formula, i, "this is a bug");
        }

        i = other_bracket + 1;

        return result;
    }

    Variable* ExprParser::parse_variable(std::string formula, int &i) {
        if (formula[i] == 'R') {
            i++;
        }

        Expr* row = NULL;
        bool c_found = false;

        while (i < formula.length()) {
            if (formula[i] == 'C') {
                if (!row) {
                    throw Exception::ParseError(formula, i, "no row specified for variable or flipped 'R' and 'C'");
                }

                c_found = true;
            } else if (formula[i] == ' ') {
            } else {
                Expr* expr = parse_formula_argument(formula, i);

                if (row) {
                    if (!c_found) {
                        delete row;
                        delete expr;
                        throw Exception::ParseError(formula, i, "no column specified for variable");
                    }

                    Variable* var = new Variable(*row, *expr);

                    delete row;
                    delete expr;

                    return var;
                } else {
                    row = expr;
                }

                continue;
            }

            i++;
        }

        delete row;

        throw Exception::ParseError(formula, i, "bad variable");
    }

    Expr* ExprParser::build_expr(Function function, Expr* left, Expr* right) {
        switch (function) {
            case ADDITION:
                return new Addition(*left, *right);
            case SUBTRACTION:
                return new Addition(*left, Negation(*right));
            case MULTIPLICATION:
                return new Multiplication(*left, *right);
            case DIVISION:
                return new Division(*left, *right);
            case POWER:
                return new Power(*left, *right);
            default:
                return NULL;
        }
    }
}
