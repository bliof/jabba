/*
 * Utils.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Utils.h"

#include <sstream>
#include <string>
#include <iostream>

namespace Jabba {
    namespace utl {
        int find_matching_closing_bracket(const std::string &str, int opening_bracket_pos) {
            int opening_brackets = 1;
            int closing_bracket_pos = -1;

            for (int i = opening_bracket_pos + 1; i < str.length(); i++) {
                if (str[i] == '(') {
                    opening_brackets++;
                } else if (str[i] == ')') {
                    opening_brackets--;

                    if (opening_brackets == 0) {
                        closing_bracket_pos = i;
                        break;
                    }
                }
            }

            return closing_bracket_pos;
        }

        size_t find_not_escaped(const std::string &string, char symbol, size_t after) {
            for (size_t i = after + 1; i < string.length(); i++) {
                if (string[i] == symbol and !is_escaped(string, i)) {
                    return i;
                }
            }

            return 0;
        }

        bool is_escaped(const std::string &string, size_t i) {
            bool escaped = false;

            if (i == 0) { return false; }

            i--;
            while (string[i] == '\\') {
                escaped = !escaped;

                if (i == 0) { break; }

                i--;
            }

            return escaped;
        }
    }
}
