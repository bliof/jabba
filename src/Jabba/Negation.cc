/*
 * Negation.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Negation.h"

#include <string>

#include "Expr.h"
#include "Unary.h"
#include "Table.h"

namespace Jabba {
    Negation::Negation(const Expr &expr)
        : Unary(expr)
    {}

    double Negation::evaluate(const Table* table) const {
        return -(expr->evaluate(table));
    }

    std::string Negation::to_string() const {
        return "-(" + expr->to_string() + ")";
    }

    Expr* Negation::clone() const {
        return new Negation(*expr);
    }

    bool Negation::is_formula() {
        return expr->is_formula();
    }
}

