/*
 * Table.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Table_h__
#define __Table_h__

#include "Expr.h"

namespace Jabba {
    class Table {
    public:
        Table() {};
        virtual ~Table() {};
        virtual Expr* get_cell(int row, int column) const = 0;
        virtual void set_cell(int row, int column, const Expr &expr) = 0;
        virtual void set_cell(int row, int column, const Expr* expr) = 0;
        virtual void clear_cell(int row, int column) = 0;
        virtual int rows_count() const = 0;
        virtual int columns_count(int row) const = 0;
    };
}

#endif /* !__Table_h__ */

