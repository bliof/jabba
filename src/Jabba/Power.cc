/*
 * Power.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Power.h"

#include <string>
#include <cmath>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    Power::Power(const Expr &left, const Expr &right)
        : Binary(left, right)
    {}

    //TODO throw some errors for 0^0
    double Power::evaluate(const Table* table) const {
        double base = left->evaluate(table);
        double exponent = right->evaluate(table);

        return std::pow(base, exponent);
    }

    std::string Power::to_string() const {
        return "(" + left->to_string() + " ^ " + right->to_string() + ")";
    }

    Expr* Power::clone() const {
        return new Power(*left, *right);
    }
}


