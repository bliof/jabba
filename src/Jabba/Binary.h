/*
 * Binary.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Binary_h__
#define __Binary_h__

#include "Expr.h"

namespace Jabba {
    class Binary : public Expr {
    public:
        Binary(const Expr &left, const Expr &right)
            : left(left.clone()),
              right(right.clone())
        {}

        virtual ~Binary() {
            delete left;
            delete right;
        }

        virtual bool is_formula() {
            return true;
        }
    protected:
        Expr *left;
        Expr *right;
    };
}

#endif /* !__Binary_h__ */

