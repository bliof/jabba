/*
 * Double.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Double.h"

#include <string>

#include "Expr.h"
#include "Utils.h"
#include "Table.h"

namespace Jabba {
    Double::Double()
        : value_(0)
    {}

    Double::Double(int value)
        : value_(value)
    {}

    Double::Double(double value)
        : value_(value)
    {}

    Double::Double(const Expr &expr) {
        value_ = expr.evaluate(NULL);
    }

    double Double::evaluate(const Table* table) const {
        return value_;
    }

    std::string Double::to_string() const {
        return utl::to_string(value_);
    }

    Expr* Double::clone() const {
        return new Double(value_);
    }
}

