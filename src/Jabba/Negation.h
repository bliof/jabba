/*
 * Negation.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Negation_h__
#define __Negation_h__

#include "Unary.h"
#include "Table.h"

namespace Jabba {
    class Negation : public Unary {
    public:
        Negation(const Expr &expr);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;

        bool is_formula();
    };
}

#endif /* !__Negation_h__ */

