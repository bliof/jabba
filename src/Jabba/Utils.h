/*
 * Utils.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Utils_h__
#define __Utils_h__

#include <sstream>
#include <string>
#include <vector>
#include <cctype>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

namespace Jabba {
    namespace utl {
        template <typename T> std::string to_string (T n) {
            std::ostringstream oss;
            oss << n;
            return oss.str();
        }

        inline std::string to_string(double n) {
            return to_string<double>(n);
        }

        inline std::string to_string(int n) {
            return to_string<int>(n);
        }

        template <typename T> T to_number(const std::string &str) {
            std::istringstream iss(str);
            T result;
            return (iss >> result) && iss.eof() ? result : 0;
        }

        template <typename T> bool is_number(const std::string &str) {
            std::istringstream iss(str);
            T n;
            return (iss >> n && iss.eof());
        }

        inline bool is_number(const std::string &str) {
            return is_number<double>(str);
        }

        inline bool is_number(const char &c) {
            return isdigit(c);
        }


        inline std::string &ltrim(std::string &s) {
            s.erase(
                s.begin(),
                std::find_if(
                    s.begin(),
                    s.end(),
                    std::not1(
                        std::ptr_fun<int, int>(std::isspace)
                    )
                )
            );
            return s;
        }

        inline std::string &rtrim(std::string &s) {
            s.erase(
                std::find_if(
                    s.rbegin(),
                    s.rend(),
                    std::not1(
                        std::ptr_fun<int, int>(std::isspace)
                    )
                ).base(),
                s.end()
            );
            return s;
        }

        inline std::string &trim(std::string &s) {
                return ltrim(rtrim(s));
        }

        int find_matching_closing_bracket(const std::string &str, int opening_bracket_pos = 0);

        size_t find_not_escaped(const std::string &string, char symbol = '"', size_t after = 0);

        bool is_escaped(const std::string &string, size_t i);
    }
}

#endif /* !__Utils_h__ */

