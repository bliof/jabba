/*
 * Unary.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Unary_h__
#define __Unary_h__

#include "Expr.h"

namespace Jabba {
    class Unary : public Expr {
    public:
        Unary()
            : expr(NULL)
        {}

        Unary(const Expr &expr)
            : expr(expr.clone())
        {}

        virtual ~Unary() {
            delete expr;
        };

        virtual bool is_formula() {
            return false;
        }
    protected:
        Expr *expr;
    };
}

#endif /* !__Unary_h__ */

