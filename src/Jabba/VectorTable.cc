/*
 * VectorTable.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "VectorTable.h"

#include <vector>

#include "Expr.h"
#include "Utils.h"

namespace Jabba {
    VectorTable::VectorTable() {}

    VectorTable::~VectorTable() {
        for (
            std::vector< std::vector< Expr* > >::iterator it_row = rows.begin();
            it_row != rows.end();
            ++it_row
        ) {
            for (
                std::vector< Expr* >::iterator it_column = (*it_row).begin();
                it_column != (*it_row).end();
                ++it_column
            ) {
                delete (*it_column);
            }
        }

        rows.clear();
    }

    Expr* VectorTable::get_cell(int row, int column) const {
        return rows.at(row).at(column);
    }

    void VectorTable::clear_cell(int row, int column) {
        Expr *expr = get_cell(row, column);
        delete expr;
    }

    void VectorTable::set_cell(int row, int column, const Expr &expr) {
        set_cell(row, column, &expr);
    }

    void VectorTable::set_cell(int row, int column, const Expr* expr) {
        if (row >= rows.size()) {
            std::vector<Expr*> vec;
            rows.resize(row + 1, vec);
        }

        std::vector<Expr*> &columns = rows[row];

        if (column >= columns.size()) {
            Expr* pointer = NULL;
            columns.resize(column + 1, pointer);
        }

        if (expr) {
            columns[column] = expr->clone();
        } else {
            columns[column] = NULL;
        }
    }

    int VectorTable::rows_count() const {
        return rows.size();
    }

    int VectorTable::columns_count(int row) const {
        return rows.at(row).size();
    }
}

