/*
 * Expr.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Expr_h__
#define __Expr_h__

#include <string>

#include "Utils.h"

namespace Jabba {
    class Addition;
    class Table;

    class Expr {
    public:
        virtual ~Expr() {}

        virtual double evaluate(const Table* table) const = 0;

        virtual std::string evaluate_to_string(const Table* table) const {
            double result = evaluate(table);

            return utl::to_string(result);
        }

        virtual std::string to_string() const { return ""; }

        virtual Expr* clone() const = 0;

        virtual bool is_formula() = 0;
    };
}

#endif /* !__Expr_h__ */

