/*
 * Addition.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Addition_h__
#define __Addition_h__

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    class Addition : public Binary {
    public:
        Addition(const Expr &left, const Expr &right);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    };
}

#endif /* !__Addition_h__ */

