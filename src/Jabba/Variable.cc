/*
 * Variable.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Variable.h"

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Int.h"
#include "Table.h"

namespace Jabba {
    Variable::Variable(int row, int column)
        : Binary(Jabba::Int(row), Jabba::Int(column))
    {}

    Variable::Variable(const Expr &left, const Expr &right)
        : Binary(left, right)
    {}

    double Variable::evaluate(const Table* table) const {
        int row = left->evaluate(table) + 0.5;
        int column = right->evaluate(table) + 0.5;

        Expr *expr = table->get_cell(row, column);

        if (expr) {
            return expr->evaluate(table);
        } else {
            return 0;
        }
    }

    std::string Variable::evaluate_to_string(const Table* table) const {
        int row = left->evaluate(table) + 0.5;
        int column = right->evaluate(table) + 0.5;

        Expr *expr = table->get_cell(row, column);

        if (expr) {
            return expr->evaluate_to_string(table);
        } else {
            return 0;
        }
    }

    std::string Variable::to_string() const {
        return "R(" + left->to_string() + ")C(" + right->to_string() + ")";
    }

    Expr* Variable::clone() const {
        return new Variable(*left, *right);
    }
}


