/*
 * ExprParser.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __ExprParser_h__
#define __ExprParser_h__

#include <string>

#include "Expr.h"
#include "Variable.h"

namespace Jabba {
    class ExprParser {
    public:
        Expr* parse(std::string raw_expr);
    private:
        enum Function {
            UNDEF,
            ADDITION, SUBTRACTION,
            MULTIPLICATION, DIVISION,
            POWER
        };

        Expr* extract_number(std::string formula, int &i);
        Expr* parse_number(std::string number);
        Expr* parse_formula(std::string formula);
        Expr* parse_formula_argument(std::string formula, int &i);
        Expr* parse_formula_in_brackets(std::string formula, int &i);
        Variable* parse_variable(std::string formula, int &i);
        Expr* build_expr(Function function, Expr* left, Expr* right);
    };
}

#endif /* !__ExprParser_h__ */
