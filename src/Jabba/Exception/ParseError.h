/*
 * ParseError.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __ParseError_h__
#define __ParseError_h__

#include <string>

#include "Base.h"

namespace Jabba {
    namespace Exception {
        class ParseError : public Base {
            public:

            size_t position;
            std::string line;
            std::string cause;

            virtual const char* what() const throw() {
                std::string error = line;
                error.insert(position, "<*>");

                if (cause == "") {
                    error = "An error occurred around <*>: " + error;
                } else {
                    error = cause + " around <*>: " + error;
                }

                return error.c_str();
            }

            virtual ~ParseError() throw() {};

            ParseError(std::string line, size_t error_pos, std::string cause = "")
                : line(line), position(error_pos), cause(cause)
            {}
        };
    }
}

#endif /* !__ParseError_h__ */

