/*
 * DivisionByZero.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __DivisionByZero_h__
#define __DivisionByZero_h__

#include "Base.h"

namespace Jabba {
    namespace Exception {
        class DivisionByZero : public Base {
            public:

            virtual const char* what() const throw() {
                return "Division by zero!!!";
            }
        };
    }
}

#endif /* !__DivisionByZero_h__ */

