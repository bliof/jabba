/*
 * Base.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Base_h__
#define __Base_h__

#include <exception>

namespace Jabba {
    namespace Exception {
        class Base : public std::exception {
            public:

            virtual const char* what() const throw() {
                return "AAAAAAA something is wrong!!";
            }
        };
    }
}

#endif /* !__Base_h__ */

