/*
 * String.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "String.h"

#include <string>

#include "Expr.h"
#include "Utils.h"
#include "Table.h"

namespace Jabba {
    String::String()
        : value_("")
    {}

    String::String(std::string s)
        : value_(s)
    {}

    String::String(const Expr &expr) {
        value_ = expr.evaluate_to_string(NULL);
    }

    double String::evaluate(const Table* table) const {
        return utl::to_number<double> (value_);
    }

    std::string String::evaluate_to_string(const Table* table) const {
        std::string result;

        for (size_t i = 0; i < value_.length(); i++) {
            if (value_[i] == '\\') {
                i++;
            }

            result += value_[i];
        }

        return result;
    }

    std::string String::to_string() const { return "\"" + value_ + "\""; };

    Expr* String::clone() const {
        return new String(value_);
    }
}

