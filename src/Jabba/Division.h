/*
 * Division.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Division_h__
#define __Division_h__

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    class Division : public Binary {
    public:
        Division(const Expr &left, const Expr &right);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    };
}

#endif /* !__Division_h__ */

