/*
 * Int.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Int_h__
#define __Int_h__

#include <string>

#include "Expr.h"
#include "Unary.h"
#include "Table.h"

namespace Jabba {
    class Int : public Unary {
    public:
        Int();
        Int(int value);
        Int(double value);
        Int(const Expr &expr);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    private:
        int value_;
    };
}

#endif /* !__Int_h__ */

