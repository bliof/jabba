/*
 * Division.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Division.h"

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"
#include "Exception/DivisionByZero.h"

namespace Jabba {
    Division::Division(const Expr &left, const Expr &right)
        : Binary(left, right)
    {}

    double Division::evaluate(const Table* table) const {
        double divisor = right->evaluate(table);

        if (divisor == 0) {
            throw Exception::DivisionByZero();
        }

        return left->evaluate(table) / divisor;
    }

    std::string Division::to_string() const {
        return "(" + left->to_string() + " / " + right->to_string() + ")";
    }

    Expr* Division::clone() const {
        return new Division(*left, *right);
    }
}


