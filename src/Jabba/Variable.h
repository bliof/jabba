/*
 * Variable.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Variable_h__
#define __Variable_h__

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    class Variable : public Binary {
    public:
        Variable(int row, int column);

        Variable(const Expr &left, const Expr &right);

        double evaluate(const Table* table) const;

        std::string evaluate_to_string(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    };
}

#endif /* !__Variable_h__ */

