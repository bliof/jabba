/*
 * Addition.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Addition.h"

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    Addition::Addition(const Expr &left, const Expr &right)
        : Binary(left, right)
    {}

    double Addition::evaluate(const Table* table) const {
        return left->evaluate(table) + right->evaluate(table);
    }

    std::string Addition::to_string() const {
        return "(" + left->to_string() + " + " + right->to_string() + ")";
    }

    Expr* Addition::clone() const {
        return new Addition(*left, *right);
    }
}

