/*
 * String.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __String_h__
#define __String_h__

#include <string>

#include "Expr.h"
#include "Unary.h"
#include "Table.h"

namespace Jabba {
    class String : public Unary {
    public:
        String();
        String(std::string s);
        String(const Expr &expr);

        double evaluate(const Table* table) const;

        std::string evaluate_to_string(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    private:
        std::string value_;
    };
}

#endif /* !__String_h__ */

