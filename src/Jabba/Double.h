/*
 * Double.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Double_h__
#define __Double_h__

#include <string>

#include "Expr.h"
#include "Unary.h"
#include "Table.h"

namespace Jabba {
    class Double : public Unary {
    public:
        Double();
        Double(int value);
        Double(double value);
        Double(const Expr &expr);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    private:
        double value_;
    };
}

#endif /* !__Double_h__ */

