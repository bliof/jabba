/*
 * Multiplication.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "Multiplication.h"

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    Multiplication::Multiplication(const Expr &left, const Expr &right)
        : Binary(left, right)
    {}

    double Multiplication::evaluate(const Table* table) const {
        return left->evaluate(table) * right->evaluate(table);
    }

    std::string Multiplication::to_string() const {
        return "(" + left->to_string() + " * " + right->to_string() + ")";
    }

    Expr* Multiplication::clone() const {
        return new Multiplication(*left, *right);
    }
}


