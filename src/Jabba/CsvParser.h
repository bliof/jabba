/*
 * CsvParser.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __CsvParser_h__
#define __CsvParser_h__

#include <vector>
#include <string>
#include <istream>
#include <queue>

#include "Table.h"
#include "ExprParser.h"

namespace Jabba {
    class CsvParser {
        ExprParser expr_parser;

        std::queue<size_t> get_formulas_separators(const std::string& line);
    public:
        void parse(std::istream& data, Table& table);
    };
}

#endif /* !__CsvParser_h__ */

