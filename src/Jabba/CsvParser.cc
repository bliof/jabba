/*
 * CsvParser.cc
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "CsvParser.h"

#include <vector>
#include <string>
#include <istream>
#include <queue>
#include <cctype>

#include "Expr.h"
#include "ExprParser.h"
#include "Utils.h"
#include "Exception/ParseError.h"

namespace Jabba {
    void CsvParser::parse(std::istream& data, Table& table) {
        size_t row = 0;
        while(data) {
            std::string line;

            std::getline(data, line);

            std::queue<size_t> separators = get_formulas_separators(line);

            {
                Expr* expr;
                if (separators.empty()) {
                    expr = expr_parser.parse(line.substr(0));
                } else {
                    expr = expr_parser.parse(line.substr(0, separators.front()));
                }

                if (expr) {
                    table.set_cell(row, 0, *expr);
                    delete expr;
                }
            }

            size_t column = 1;

            while(!separators.empty()) {
                size_t current_separator = separators.front();
                separators.pop();

                Expr* expr;

                size_t till;
                if (!separators.empty()) {
                    till = separators.front() - current_separator - 1;
                } else {
                    till = std::string::npos;
                }

                expr = expr_parser.parse(
                    line.substr(current_separator + 1, till)
                );

                if (expr) {
                    table.set_cell(row, column, *expr);
                    delete expr;
                }

                column++;
            }

            row++;
        }
    };

    std::queue<size_t> CsvParser::get_formulas_separators(const std::string& line) {
        std::queue<size_t> separators;

        bool in_cell = false;
        bool is_last_quote = false;

        for (size_t i = 0; i < line.length(); i++) {
            if (line[i] == ',') {
                separators.push(i);
                in_cell = false;
                is_last_quote = false;
                continue;
            }

            if (std::isspace(line[i])) {
                continue;
            }

            if (is_last_quote) {
                throw Exception::ParseError(line, i, "no separator");
            }

            if (line[i] == '"' && !utl::is_escaped(line, i)) {
                if (in_cell) {
                    throw Exception::ParseError(line, i, "no separator");
                }

                size_t other = utl::find_not_escaped(line, '"', i);

                if (other == 0) {
                    throw Exception::ParseError(line, i, "no matching quote");
                }

                i = other;

                is_last_quote = true;
            }

            in_cell = true;
        }

        return separators;
    }
}

