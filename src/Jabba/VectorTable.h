/*
 * VectorTable.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __VectorTable_h__
#define __VectorTable_h__

#include <vector>

#include "Expr.h"
#include "Table.h"

/*
 * The table is represented as a vector of rows that contain vector of columns
 */

namespace Jabba {
    class VectorTable : public Table {
    public:
        VectorTable();
        ~VectorTable();

        Expr* get_cell(int row, int column) const;
        void set_cell(int row, int column, const Expr &expr);
        void set_cell(int row, int column, const Expr* expr);
        void clear_cell(int row, int column);
        int rows_count() const;
        int columns_count(int row) const;
    private:
        std::vector< std::vector<Expr*> > rows;
    };
}

#endif /* !__VectorTable_h__ */

