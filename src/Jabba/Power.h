/*
 * Power.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __Power_h__
#define __Power_h__

#include <string>

#include "Expr.h"
#include "Binary.h"
#include "Table.h"

namespace Jabba {
    class Power : public Binary {
    public:
        Power(const Expr &left, const Expr &right);

        double evaluate(const Table* table) const;

        std::string to_string() const;

        Expr* clone() const;
    };
}

#endif /* !__Power_h__ */

