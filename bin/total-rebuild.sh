#!/bin/bash

set -e

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd $DIR/..

rm -rf build
git checkout build

cd build

cmake ..
make
make test

#bin/tests/test_expr_parser
unset DIR
