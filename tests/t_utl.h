/*
 * TUtils.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __T_UTL_h__
#define __T_UTL_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Expr.h"

namespace t_utl {
    inline void evaluates_to(const Jabba::Expr &expr, double expect) {
        double result = expr.evaluate(NULL);

        TS_ASSERT_EQUALS(result, expect);
    }

    inline void evaluates_to_string(const Jabba::Expr &expr, std::string expect) {
        std::string result = expr.evaluate_to_string(NULL);

        TS_ASSERT_EQUALS(result, expect);
    }

    inline void evaluates_to(const Jabba::Expr *expr, double expect) {
        if (expr) {
            evaluates_to(*expr, expect);
        } else {
            TS_FAIL("null pointer passed to evaluates_to");
        }
    }

    inline void evaluates_to_string(const Jabba::Expr *expr, std::string expect) {
        if (expr) {
            evaluates_to_string(*expr, expect);
        } else {
            TS_FAIL("null pointer passed to evaluates_to_string");
        }
    }

    template <class DstType>
    bool instance_of(const Jabba::Expr* src) {
        return dynamic_cast<const DstType*>(src) != 0;
    }

    template <class SrcType, class DstType>
    bool instance_of(const SrcType* src) {
      return dynamic_cast<const DstType*>(src) != 0;
    }
}

#endif /* !__T_UTL_h__ */


