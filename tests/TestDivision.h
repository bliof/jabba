/*
 * TestDivision.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestDivision_h__
#define __TestDivision_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Division.h"
#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/String.h"
#include "Jabba/Exception/DivisionByZero.h"
#include "t_utl.h"

class TestDivision : public CxxTest::TestSuite {
public:
    void test_evaluate_with_int_and_double() {
        Jabba::Division expr(Jabba::Int(100), Jabba::Double(10));

        t_utl::evaluates_to(expr, 10);
    }

    void test_evaluate_with_int_and_string() {
        Jabba::Division expr(Jabba::Int(23), Jabba::String("5"));

        t_utl::evaluates_to(expr, 4.6);
    }

    void test_evaluate_to_string_with_int_and_string() {
        Jabba::Division expr(Jabba::Int(600), Jabba::String("100"));

        t_utl::evaluates_to_string(expr, "6");
    }

    void test_evaluate_divide_by_zero() {
        Jabba::Division expr(Jabba::Int(600), Jabba::Int(0));

        TS_ASSERT_THROWS(expr.evaluate(NULL), Jabba::Exception::DivisionByZero);
    }
};

#endif /* !__TestDivision_h__ */

