/*
 * TestAddition.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestAddition_h__
#define __TestAddition_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Addition.h"
#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/String.h"
#include "t_utl.h"

class TestAddition : public CxxTest::TestSuite {
public:
    void test_evaluate_with_int_and_double() {
        Jabba::Addition expr(Jabba::Int(10), Jabba::Double(12.30));

        t_utl::evaluates_to(expr, 22.30);
    }

    void test_evaluate_with_int_and_string() {
        Jabba::Addition expr(Jabba::Int(10), Jabba::String("30"));

        t_utl::evaluates_to(expr, 40);
    }

    void test_evaluate_to_string_with_int_and_string() {
        Jabba::Addition expr(Jabba::Int(10), Jabba::String("30"));

        t_utl::evaluates_to_string(expr, "40");
    }
};

#endif /* !__TestAddition_h__ */

