/*
 * TestPower.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestPower_h__
#define __TestPower_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Power.h"
#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/String.h"
#include "t_utl.h"

class TestPower : public CxxTest::TestSuite {
public:
    void test_evaluate_with_int_and_double() {
        Jabba::Power expr(Jabba::Int(10), Jabba::Double(2));

        t_utl::evaluates_to(expr, 100);
    }

    void test_evaluate_with_int_and_string() {
        Jabba::Power expr(Jabba::Int(2), Jabba::String("5"));

        t_utl::evaluates_to(expr, 32);
    }

    void test_evaluate_to_string_with_int_and_string() {
        Jabba::Power expr(Jabba::Int(3), Jabba::String("3"));

        t_utl::evaluates_to_string(expr, "27");
    }

    void test_power_of_0() {
        Jabba::Power expr(Jabba::Int(6), Jabba::Int(0));

        t_utl::evaluates_to(expr, 1);
    }

    void test_negative_base_to_odd_power() {
        Jabba::Power expr(Jabba::Int(-3), Jabba::Int(3));

        t_utl::evaluates_to(expr, -27);
    }

    void test_negative_base_to_even_power() {
        Jabba::Power expr2(Jabba::Int(-3), Jabba::Int(2));

        t_utl::evaluates_to(expr2, 9);
    }
};

#endif /* !__TestPower_h__ */

