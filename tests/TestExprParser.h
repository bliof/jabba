/*
 * TestExprParser.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestExprParser_h__
#define __TestExprParser_h__

#include <cxxtest/TestSuite.h>

#include "Jabba/ExprParser.h"
#include "Jabba/Expr.h"
#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/String.h"
#include "Jabba/Variable.h"
#include "Jabba/Addition.h"
#include "Jabba/Multiplication.h"
#include "Jabba/Division.h"
#include "Jabba/Power.h"
#include "Jabba/VectorTable.h"
#include "t_utl.h"

#include "Jabba/Exception/ParseError.h"

class TestExprParser : public CxxTest::TestSuite {
public:
    //Basic tests for `parse`/*{{{*/

    void test_parse_int() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("123");

        TS_ASSERT(t_utl::instance_of<Jabba::Int>(expr));
        t_utl::evaluates_to(expr, 123);

        delete expr;
    }

    void test_parse_int_with_spaces() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("     200          ");

        t_utl::evaluates_to(expr, 200);
        delete expr;
    }

    void test_parse_double() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("1.54");

        TS_ASSERT(t_utl::instance_of<Jabba::Double>(expr));
        t_utl::evaluates_to(expr, 1.54);

        delete expr;
    }

    void test_parse_string() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("some string");

        TS_ASSERT(t_utl::instance_of<Jabba::String>(expr));

        delete expr;
    }

    void test_parse_quoted_string() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("\"123\"");

        TS_ASSERT(t_utl::instance_of<Jabba::String>(expr));

        delete expr;
    }

    void test_parse_variable() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=R100C200");

        TS_ASSERT(t_utl::instance_of<Jabba::Variable>(expr));

        delete expr;
    }

    void test_parse_addition() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=10 + 25");

        TS_ASSERT(t_utl::instance_of<Jabba::Addition>(expr));
        t_utl::evaluates_to(expr, 35);

        delete expr;
    }

    void test_parse_addition_with_double() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=10 + 25.5");

        TS_ASSERT(t_utl::instance_of<Jabba::Addition>(expr));
        t_utl::evaluates_to(expr, 35.5);

        delete expr;
    }

    void test_parse_substraction() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=10 - 25");

        t_utl::evaluates_to(expr, -15);

        delete expr;
    }

    void test_parse_multiplication() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=10 * 25");

        TS_ASSERT(t_utl::instance_of<Jabba::Multiplication>(expr));
        t_utl::evaluates_to(expr, 250);

        delete expr;
    }

    void test_parse_division() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=25 / 5");

        TS_ASSERT(t_utl::instance_of<Jabba::Division>(expr));
        t_utl::evaluates_to(expr, 5);

        delete expr;
    }

    void test_parse_power() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=11 ^ 2");

        TS_ASSERT(t_utl::instance_of<Jabba::Power>(expr));
        t_utl::evaluates_to(expr, 121);

        delete expr;
    }

    void test_parse_with_brackets() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=(11 ^ 2)");

        TS_ASSERT(t_utl::instance_of<Jabba::Power>(expr));
        t_utl::evaluates_to(expr, 121);

        delete expr;
    }

    void test_parse_with_nested_brackets() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=((11 ^ 2))");

        TS_ASSERT(t_utl::instance_of<Jabba::Power>(expr));
        t_utl::evaluates_to(expr, 121);

        delete expr;
    }

    void test_parse_with_basic_variable() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=R2C3");

        TS_ASSERT(t_utl::instance_of<Jabba::Variable>(expr));
        TS_ASSERT_EQUALS(expr->to_string(), "R(2)C(3)");

        delete expr;
    }

    void test_parse_with_variable_with_brackets() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=R(100)C(200)");

        TS_ASSERT(t_utl::instance_of<Jabba::Variable>(expr));

        delete expr;
    }

    /*}}}*/

    // Complex tests for `parse`/*{{{*/

    void test_formulas_with_brackets() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=(25 / 5) + 5");

        t_utl::evaluates_to(expr, 10);

        delete expr;
    }

    void test_formulas_with_multiple_brackets() {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = parser.parse("=(25 / 5) + (100/10)");

        t_utl::evaluates_to(expr, 15);

        delete expr;
    }

    void test_formulas_with_variables() {
        Jabba::ExprParser parser;
        Jabba::VectorTable table;

        table.set_cell(0, 0, Jabba::Int(3));
        table.set_cell(0, 1, Jabba::Int(2));

        Jabba::Expr* expr = parser.parse("=R0C0 + R0C1");

        TS_ASSERT_EQUALS(expr->evaluate(&table), 5);

        delete expr;
    }

    void test_complex_formulas_with_variables() {
        Jabba::ExprParser parser;
        Jabba::VectorTable table;

        table.set_cell(0, 0, Jabba::Int(3));
        table.set_cell(0, 1, Jabba::Int(9));

        Jabba::Expr* expr = parser.parse("=(((R0C0^2) + R0C1)/(1+2))*5");

        TS_ASSERT_EQUALS(expr->evaluate(&table), 30);

        delete expr;
    }

    void test_complex_formulas_with_variables_with_random_spaces() {
        Jabba::ExprParser parser;
        Jabba::VectorTable table;

        table.set_cell(0, 0, Jabba::Int(3));
        table.set_cell(0, 1, Jabba::Int(9));

        Jabba::Expr* expr = parser.parse("=( ((R0C0 ^2 ) + R0C1) /( 1+2))*5");

        TS_ASSERT_EQUALS(expr->evaluate(&table), 30);

        delete expr;
    }

    void test_formula_with_all_exprs() {
        Jabba::ExprParser parser;
        Jabba::VectorTable table;

        Jabba::Expr* expr = parser.parse(
            "=(((R0C0 + (3^R0C1)) * (5 - 2))/2) + 1.5 "
        );

        table.set_cell(0, 0, Jabba::Int(3));
        table.set_cell(0, 1, Jabba::Int(2));

        TS_ASSERT_EQUALS(expr->evaluate(&table), 19.5);

        delete expr;
    }

    void test_bug_with_variable_in_brackets() {
        Jabba::ExprParser parser;
        Jabba::VectorTable table;

        Jabba::Expr* expr = parser.parse(
            "=0-(R1C0)"
        );

        table.set_cell(1, 0, Jabba::Int(2));

        TS_ASSERT_EQUALS(expr->evaluate(&table), -2);

        delete expr;
    }

    /*}}}*/

    //Test error handling/*{{{*/

    void assert_parser_error_for(const char* str) {
        Jabba::ExprParser parser;

        Jabba::Expr* expr = NULL;
        TS_ASSERT_THROWS(expr = parser.parse(str), Jabba::Exception::ParseError);

        if (expr) {
            TS_WARN("returned " + expr->to_string());

            delete expr;
        }
    }

    void test_basic_wrong_formulas() {
        assert_parser_error_for("=+10");
        assert_parser_error_for("=10+");
        assert_parser_error_for("=*10");
        assert_parser_error_for("=10/");
        assert_parser_error_for("=10-");
        assert_parser_error_for("=10^");
        assert_parser_error_for("=R0");
        assert_parser_error_for("=RC0");
        assert_parser_error_for("=R0C0 + (100");
        assert_parser_error_for("=100 / a");
        assert_parser_error_for("=100 / 1)");
    }

    /*}}}*/
};

#endif /* !__TestExprParser_h__ */
