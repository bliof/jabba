/*
 * TestString.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestString_h__
#define __TestString_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/String.h"
#include "Jabba/Double.h"
#include "t_utl.h"

class TestString : public CxxTest::TestSuite {
public:
    void test_evaluate() {
        Jabba::String n("12.12");

        t_utl::evaluates_to(n, 12.12);
    }

    void test_evaluate_with_number_at_the_end() {
        Jabba::String n("12.1adf");

        t_utl::evaluates_to(n, 0);
    }

    void test_evaluate_with_number_at_the_start() {
        Jabba::String n("asd12.1");

        t_utl::evaluates_to(n, 0);
    }

    void test_evaluate_with_number_in_the_middle() {
        Jabba::String n("12a1");

        t_utl::evaluates_to(n, 0);
    }

    void test_evaluate_with_two_dots() {
        Jabba::String n("121.12.123");

        t_utl::evaluates_to(n, 0);
    }

    void test_evaluate_with_int() {
        Jabba::String n("121");

        t_utl::evaluates_to(n, 121);
    }

    void test_evaluate_to_string() {
        Jabba::String n("Hello world!");

        t_utl::evaluates_to_string(n, "Hello world!");
    }

    void test_create_with_another_expr() {
        Jabba::String n(Jabba::Double(10.010));

        t_utl::evaluates_to_string(n, "10.01");
    }
};

#endif /* !__TestString_h__ */

