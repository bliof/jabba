/*
 * TestVariable.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestVariable_h__
#define __TestVariable_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Variable.h"
#include "Jabba/VectorTable.h"
#include "Jabba/Int.h"
#include "Jabba/String.h"
#include "t_utl.h"

class TestVariable : public CxxTest::TestSuite {
public:
    void test_evaluate() {
        Jabba::Variable var(1, 1);

        Jabba::VectorTable table;

        table.set_cell(1, 1, Jabba::Int(15));

        double result = var.evaluate(&table);

        TS_ASSERT_EQUALS(result, 15);
    }

    void test_evaluate_to_string() {
        Jabba::Variable var(1, 1);

        Jabba::VectorTable table;

        table.set_cell(1, 1, Jabba::Int(15));

        std::string result = var.evaluate_to_string(&table);

        TS_ASSERT_EQUALS(result, "15");
    }

    void test_evaluate_to_string_with_link_to_string_field() {
        Jabba::Variable var(1, 1);

        Jabba::VectorTable table;

        table.set_cell(1, 1, Jabba::String("ninja style"));

        std::string result = var.evaluate_to_string(&table);

        TS_ASSERT_EQUALS(result, "ninja style");
    }
};

#endif /* !__TestVariable_h__ */

