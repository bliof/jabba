/*
 * TestCsvParser.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestCsvParser_h__
#define __TestCsvParser_h__


#include <cxxtest/TestSuite.h>
#include <string>
#include <sstream>

#include "Jabba/CsvParser.h"
#include "Jabba/VectorTable.h"

#include "Jabba/Expr.h"


class TestCsvParser : public CxxTest::TestSuite {
public:
    void test_parsing_one_cell() {
        Jabba::CsvParser parser;
        Jabba::VectorTable table;

        std::istringstream iss("=10 + 30");

        parser.parse(iss, table);

        TS_ASSERT_EQUALS(table.get_cell(0, 0)->evaluate(&table), 40);
    }

    void test_parsing_multiple_columns() {
        Jabba::CsvParser parser;
        Jabba::VectorTable table;

        std::istringstream iss("=10 + 30, \"Hello\",=R0C0 / 2");

        parser.parse(iss, table);

        //TS_WARN(table.get_cell(0, 0)->to_string());
        TS_ASSERT_EQUALS(table.get_cell(0, 0)->evaluate(&table), 40);

        //TS_WARN(table.get_cell(0, 1)->to_string());
        TS_ASSERT_EQUALS(table.get_cell(0, 1)->evaluate_to_string(&table), "Hello");

        //TS_WARN(table.get_cell(0, 2)->to_string());
        TS_ASSERT_EQUALS(table.get_cell(0, 2)->evaluate(&table), 20);
    }

    void test_parsing_multiple_rows() {
        Jabba::CsvParser parser;
        Jabba::VectorTable table;

        std::istringstream iss(
            std::string("=10 + 30, \"Hello\",=R0C0 / 2\n") +
            "32\n" +
            "=R1C0 - 2"
        );

        parser.parse(iss, table);

        TS_ASSERT_EQUALS(table.get_cell(0, 0)->evaluate(&table), 40);
        TS_ASSERT_EQUALS(table.get_cell(0, 1)->evaluate_to_string(&table), "Hello");
        TS_ASSERT_EQUALS(table.get_cell(0, 2)->evaluate(&table), 20);
        TS_ASSERT_EQUALS(table.get_cell(1, 0)->evaluate(&table), 32);
        TS_ASSERT_EQUALS(table.get_cell(2, 0)->evaluate(&table), 30);
    }

    void test_parsing_with_escaped_characters() {
        Jabba::CsvParser parser;
        Jabba::VectorTable table;

        std::istringstream iss("=10 + 30, \"\\\"Hello\\\"\",=R0C0 / 2");

        parser.parse(iss, table);

        TS_ASSERT_EQUALS(table.get_cell(0, 0)->evaluate(&table), 40);
        TS_ASSERT_EQUALS(table.get_cell(0, 1)->evaluate_to_string(&table), "\"Hello\"");
        TS_ASSERT_EQUALS(table.get_cell(0, 2)->evaluate(&table), 20);
    }

    void test_the_bug_with_cell_separating() {
        Jabba::CsvParser parser;
        Jabba::VectorTable table;

        std::istringstream iss(
            std::string("Hello, World !!!,Hi\n") +
            "=10 - 5, =0-(R1C0)\n"
        );

        parser.parse(iss, table);

        TS_ASSERT_EQUALS(table.get_cell(1, 1)->evaluate(&table), -5);
    }
};

#endif /* !__TestCsvParser_h__ */

