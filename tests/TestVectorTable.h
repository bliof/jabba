/*
 * TestVectorTable.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestVectorTable_h__
#define __TestVectorTable_h__

#include <cxxtest/TestSuite.h>

#include "Jabba/VectorTable.h"
#include "Jabba/Int.h"

class TestVectorTable : public CxxTest::TestSuite {
public:
    void test_set_cell() {
        Jabba::VectorTable table;

        table.set_cell(5, 5, Jabba::Int(10));

        Jabba::Expr* expr = table.get_cell(5, 5);

        TS_ASSERT_EQUALS(expr->evaluate(&table), 10);

        Jabba::Expr* pointer = table.get_cell(5, 0);

        TS_ASSERT_EQUALS(pointer, (Jabba::Expr*) NULL);
    }

    void test_set_multiple_cells() {
        Jabba::VectorTable table;

        table.set_cell(5, 5, Jabba::Int(1));
        table.set_cell(2, 3, Jabba::Int(2));
        table.set_cell(1, 1, Jabba::Int(3));

        {
            Jabba::Expr* expr = table.get_cell(5, 5);
            TS_ASSERT_EQUALS(expr->evaluate(&table), 1);
        }

        {
            Jabba::Expr* expr = table.get_cell(2, 3);
            TS_ASSERT_EQUALS(expr->evaluate(&table), 2);
        }

        {
            Jabba::Expr* expr = table.get_cell(1, 1);
            TS_ASSERT_EQUALS(expr->evaluate(&table), 3);
        }
    }

    void test_set_zero_zero_cell() {
        Jabba::VectorTable table;
        table.set_cell(0, 0, Jabba::Int(1));

        Jabba::Expr* expr = table.get_cell(0, 0);
        TS_ASSERT_EQUALS(expr->evaluate(&table), 1);
    }
};

#endif /* !__TestVectorTable_h__ */

