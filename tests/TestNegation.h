/*
 * TestNegation.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestNegation_h__
#define __TestNegation_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Negation.h"
#include "Jabba/Int.h"
#include "t_utl.h"

class TestNegation : public CxxTest::TestSuite {
public:
    void test_evaluate() {
        Jabba::Negation expr(Jabba::Int(12));

        t_utl::evaluates_to(expr, -12);
    }
};

#endif /* !__TestNegation_h__ */

