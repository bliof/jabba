/*
 * TestMultiplication.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestMultiplication_h__
#define __TestMultiplication_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Multiplication.h"
#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/String.h"
#include "t_utl.h"

class TestMultiplication : public CxxTest::TestSuite {
public:
    void test_evaluate_with_int_and_double() {
        Jabba::Multiplication expr(Jabba::Int(10), Jabba::Double(12.30));

        t_utl::evaluates_to(expr, 123);
    }

    void test_evaluate_with_int_and_string() {
        Jabba::Multiplication expr(Jabba::Int(10), Jabba::String("30"));

        t_utl::evaluates_to(expr, 300);
    }

    void test_evaluate_to_string_with_int_and_string() {
        Jabba::Multiplication expr(Jabba::Int(10), Jabba::String("30"));

        t_utl::evaluates_to_string(expr, "300");
    }
};

#endif /* !__TestMultiplication_h__ */

