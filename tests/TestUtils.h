/*
 * TestUtils.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestUtils_h__
#define __TestUtils_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Utils.h"

class TestUtils : public CxxTest::TestSuite {
public:
    void test_find_matching_bracket() {
        std::string s = "(four)";

        TS_ASSERT_EQUALS(Jabba::utl::find_matching_closing_bracket(s), 5);
    }

    void test_find_matching_bracket_from_multiple_brackets() {
        std::string s = "(f (o) (u) r) test)";

        TS_ASSERT_EQUALS(Jabba::utl::find_matching_closing_bracket(s), 12);
    }

    void test_find_matching_bracket_with_nested_ones() {
        std::string s = "((test))";

        TS_ASSERT_EQUALS(Jabba::utl::find_matching_closing_bracket(s), 7);
    }

    void test_is_escaped() {
        TS_ASSERT( Jabba::utl::is_escaped("\\a", 1));
        TS_ASSERT(!Jabba::utl::is_escaped("\\\\a", 2));
        TS_ASSERT( Jabba::utl::is_escaped("\\\\\\a", 3));
        TS_ASSERT(!Jabba::utl::is_escaped("\\\\\\\\a", 4));

        TS_ASSERT( Jabba::utl::is_escaped("this is \\a", 9));
        TS_ASSERT(!Jabba::utl::is_escaped("this is \\\\a you known", 10));
        TS_ASSERT( Jabba::utl::is_escaped("this is \\\\\\a lalala", 11));
        TS_ASSERT(!Jabba::utl::is_escaped("this is \\\\\\\\a", 12));
    }
};

#endif /* !__TestUtils_h__ */

