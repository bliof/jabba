/*
 * TestInt.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestInt_h__
#define __TestInt_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Int.h"
#include "Jabba/Double.h"
#include "Jabba/Table.h"
#include "t_utl.h"

class TestInt : public CxxTest::TestSuite {
public:
    void test_evaluate() {
        Jabba::Int n(12);

        t_utl::evaluates_to(n, 12);
    }

    void test_evaluate_to_string() {
        Jabba::Int n(12);

        t_utl::evaluates_to_string(n, "12");
    }

    void test_create_with_another_expr() {
        Jabba::Int n(Jabba::Double(10.010));

        t_utl::evaluates_to(n, 10);
    }
};

#endif /* !__TestInt_h__ */

