/*
 * TestDouble.h
 * Copyright (C) 2013 Aleksandar Ivanov <aivanov92@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef __TestDouble_h__
#define __TestDouble_h__

#include <cxxtest/TestSuite.h>
#include <string>

#include "Jabba/Double.h"
#include "Jabba/Int.h"
#include "t_utl.h"

class TestDouble : public CxxTest::TestSuite {
public:
    void test_evaluate() {
        Jabba::Double n(12.21);

        t_utl::evaluates_to(n, 12.21);
    }

    void test_evaluate_to_string() {
        Jabba::Double n(12.3);

        t_utl::evaluates_to_string(n, "12.3");
    }

    void test_create_with_another_expr() {
        Jabba::Double n(Jabba::Int(10));

        t_utl::evaluates_to(n, 10);
    }
};

#endif /* !__TestDouble_h__ */

